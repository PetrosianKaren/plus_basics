//
//  main.cpp
//  FirstLab
//
//  Created by Karen Petrosian on 18.09.2020.
//  Copyright © 2020 Karen Petrosian. All rights reserved.
//

#include <iostream>
#include <cmath>
using namespace std;
int main(int argc, const char * argv[]) {
                                            // Задание 2.1
    double a, b, c, x1, x2;
    cout << "Введите значение a: ";
    cin >> a;
    cout << "Введите значение b: ";
    cin >> b;
    cout << "Введите значение c: ";
    cin >> c;
    
    if((b*b - 4*a*c) >= 0) //Если дискриминант больше или равен 0
    {
        x1=(-1*b + sqrt(b*b-4*a*c) )/(2 * a);
        x2=(-1*b - sqrt(b*b-4*a*c) )/(2 * a);
        cout << "Корни: " << x1 << ", " << x2 << endl << endl;
    }else{
        cout << "Дискриминант меньше 0, корни невещественные." << endl;
    }
                                             //Задание 2.2
    double x, y, t;
    cout<< "Введите x: ";
    cin >> x;
    cout<< "Введите y: ";
    cin >> y;
    t = x;
    if(y > t)
        t = y;
    cout <<"Большее из двух: "<< t << endl;
    
    return 0;
}
